package juego;

import java.awt.Color; 

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Pelota pelota;
	private double velocidad;
	private double tiempo;

	private Barra barra;
	private Barra barra2;
	private Puntos misPuntos;
	private Puntos rivalPuntos;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "PONG", 800, 600);
		this.velocidad = 3;
		this.tiempo = 0;
		this.pelota = new Pelota(entorno.getWidth()/2, entorno.getHeight()/2,velocidad);
		this.barra = new Barra(entorno.getWidth()/2, entorno.getHeight(), Color.cyan);
		this.barra2 = new Barra(entorno.getWidth()/2+150, 0);
		this.misPuntos = new Puntos(20, 580); //PORQUE NO PUEDO USAR ALTO Y ANCHO DE ENTORNO
		this.rivalPuntos = new Puntos(630, 40); //PORQUE NO PUEDO USAR ALTO Y ANCHO DE ENTORNO
		// Inicializar lo que haga falta para el juego
		// ...

		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick() {
		tiempo++;
		// Procesamiento de un instante de tiempo
		// ...
		entorno.cambiarFont("Arial", 25, Color.white);
		entorno.escribirTexto("valores: " + ""+(pelota.getX() - pelota.getTamaño()/2), 50, 50);	
		entorno.escribirTexto("valores: " + ""+(pelota.getAngulo() ), 50, 80);	

		this.misPuntos.mostrarMiContador(entorno);
		this.rivalPuntos.mostrarContadorRival(entorno);
		this.pelota.dibujarPelota(entorno);
		this.pelota.moverPelota();
	    this.pelota.choqueLateral(entorno);
	    this.barra.dibujarBarra(entorno);
		this.barra.moverB(entorno);
		this.barra2.dibujarBarra(entorno);
		//Barra.moverSola(entorno, barra2);
		this.pelota.choqueObjeto(barra);
		this.pelota.choqueObjeto2(barra2);
		if(this.pelota.getY()+this.pelota.getTamaño()/2>entorno.alto()) {
			pelota = null;
			this.pelota = new Pelota(entorno.getWidth()/2, entorno.getHeight()/2,velocidad);
			this.rivalPuntos.Sumar();

		}
		if (this.pelota.getY()-this.pelota.getTamaño()/2< 0 ) {
			pelota = null;
			this.pelota = new Pelota(entorno.getWidth()/2, entorno.getHeight()/2,velocidad);
			this.misPuntos.Sumar();
		}
		if(entorno.estaPresionada('d')) {
			this.barra2.avanzar();
		}
		if(entorno.estaPresionada('a')) {
			this.barra2.retrazar();
		}
		System.out.println(tiempo/60);
		System.out.println((tiempo/60)%2 == 0);

		if((tiempo/60)%5 == 0) {
			System.out.println((tiempo/60)/2 == 0);

			pelota.aumentarVelocidad();
		}
		}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
