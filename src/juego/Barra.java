package juego;

import java.awt.Color; 
import java.awt.Rectangle;
import entorno.Entorno;

public class Barra {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private Color color;
	private double angulo ;
	Juego juego;
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
	public double getVelocidad() {
		return velocidad;
	}
	public Color getColor() {
		return color;
	}
	public double getAngulo() {
		return angulo;
	}
	
	Barra(int X, int Y, Color C){
		this.ancho = 150;
		this.alto = 15;
		this.x = X;
		this.y = Y-(this.alto*3);
		this.velocidad = 10;
		this.color = C;
	}
	
	Barra(int X, int Y){
		this.ancho = 150;
		this.alto = 15;
		this.x = X;
		this.y = Y + this.alto/2;
		this.velocidad = 10;
		this.color = Color.red;
		this.angulo = Math.PI*2;
	}
	
	public void dibujarBarra(Entorno e) {
		e.dibujarRectangulo(this.getX(), this.getY(), this.getAncho(), this.getAlto(), 0, this.getColor());
	}
	
	public Rectangle size() {
		return new Rectangle((int)this.getX(), (int)this.getY(), (int)this.getAncho(), (int)this.getAlto()); 
	}
	
	public void moverB(Entorno e) {
		if(e.estaPresionada(e.TECLA_DERECHA) && this.getX()+ this.ancho/2 + 15 < e.getWidth())
			this.aumentarVelocidad();
		if(e.estaPresionada(e.TECLA_IZQUIERDA) && this.getX() - this.ancho/2 > 0)
			this.disminuirVelocidad();
	}
	private void disminuirVelocidad() {
		this.x-=6;
	}
	private void aumentarVelocidad() {
		this.x+= 6;
	}

	public void avanzar() {
		this.aumentarVelocidad();
	}

	public void retrazar() {
		this.disminuirVelocidad();
	}
	
//	public static void moverSola(Entorno e , Barra b) {
//		b.x += Math.cos(b.angulo) * b.velocidad;
//		if (b.getX()+ b.ancho/2 + 10 > e.getWidth())
//			b.angulo = Math.PI;
//		if (b.getX() - b.ancho/2 < 0)
//			b.angulo = Math.PI*2;
//	}
	
	

}
