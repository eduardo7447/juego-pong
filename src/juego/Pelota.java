package juego;
import java.awt.*; 
import entorno.Entorno;

public class Pelota {
	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private double angulo;
	Barra barra;
		
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getVelocidad() {
		return velocidad;
	}
	public double getTamaño() {
		return tamaño;
	}
	public double getAngulo() {
		return angulo;
	}
	
	Pelota (double X, double Y,double velocidad){
		this.x = X;
		this.y = Y;
		this.velocidad = velocidad;	
		this.tamaño = 30;
		this.angulo = Math.PI/4;
	}

	public void dibujarPelota(Entorno e){
		if (this.angulo==Math.PI/4 || this.angulo==3*Math.PI/4)
			e.dibujarCirculo(this.getX(), this.getY(), this.getTamaño(), Color.red);
		else
			e.dibujarCirculo(this.getX(), this.getY(), this.getTamaño(), Color.cyan);
	}
	
	public void moverPelota() {
			this.x += Math.cos(angulo) * this.velocidad;
			this.y += Math.sin(angulo) * this.velocidad;
	}
	
	public void choqueLateral(Entorno e) {
		if (this.getX() - this.getTamaño()/2 < 0 && this.getAngulo()==3*Math.PI/4 ) //IZQUIERDA ABAJO
			this.angulo = Math.PI/4 ;
		if (this.getX() - this.getTamaño()/2 < 0 && this.getAngulo()==5*Math.PI/4) //IZQUIERDA ARRIBA
			this.angulo = 7*Math.PI/4;
		if (this.getX() + this.getTamaño()/2 > e.getWidth()-15 && this.getAngulo()==7*Math.PI/4) //DERECHA ARRIBA
			this.angulo = 5*Math.PI/4 ;
		if (this.getX() + this.getTamaño()/2 > e.getWidth()-15 && this.getAngulo()==Math.PI/4 ) //DERECHA ABAJO
			this.angulo = 3*Math.PI/4;
	}
	
	public void choqueObjeto(Barra b) {	
			if ( this.getY() + this.getTamaño()/2  < b.getY() +2.5 && this.getY() + this.getTamaño()/2 +2 > b.getY() && this.getAngulo()==3*Math.PI/4 && (this.getX() + this.getTamaño()/2 >= (b.getX() - b.getAncho()/2) )) //IZQUIERDA ABAJO
				this.angulo = 5*Math.PI/4 ;
			if ( this.getY() + this.getTamaño()/2  < b.getY() +2.5 && this.getY() + this.getTamaño()/2 + 2 > b.getY() && this.getAngulo()==Math.PI/4 &&  (this.getX() - this.getTamaño()/2 + 2 <= (b.getX() + b.getAncho()/2) )) //DERECHA ABAJO
				this.angulo = 7*Math.PI/4;
		}
	
	public void choqueObjeto2(Barra b) {
		if ( this.getY() - this.getTamaño()/2  < b.getY() + b.getAlto()/2 && this.getY() - (this.getTamaño()/2) > b.getY() + (b.getAlto()/2-2) && this.getAngulo()==5*Math.PI/4 && (this.getX() + this.getTamaño()/2 >= (b.getX() - b.getAncho()/2) )) //IZQUIERDA ABAJO
			this.angulo = 3*Math.PI/4 ;
		if ( this.getY() - this.getTamaño()/2  < b.getY() + b.getAlto()/2 && this.getY() - (this.getTamaño()/2) > b.getY() + (b.getAlto()/2 -2) && this.getAngulo()==7*Math.PI/4 && (this.getX() - this.getTamaño()/2 <= (b.getX() + b.getAncho()/2) )) //DERECHA ABAJO
			this.angulo = Math.PI/4;
		}
	public void aumentarVelocidad() {
		this.velocidad++;
	}
	
}