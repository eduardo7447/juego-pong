package juego;

import java.awt.Color;

import entorno.Entorno;

public class Puntos {
	private double x;
	private double y;
	private int contador;
	Pelota pelota;
	
	
	Puntos(int X, int Y){
		this.x = X;
		this.y = Y;
		this.contador = 0;
	}
	
	public void mostrarMiContador(Entorno e) {
    	e.cambiarFont("Arial", 25, Color.white);
		e.escribirTexto("Mis puntos: " + Integer.toString(contador), this.x, this.y);
	}
	
	public void mostrarContadorRival(Entorno e) {
    	e.cambiarFont("Arial", 25, Color.white);
		e.escribirTexto("Punto rival: " + Integer.toString(contador), this.x, this.y);
	}
	
	public void Sumar() {
		this.contador += 1;
	}
}
